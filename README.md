# Rooting Method of Huawei Y7 Prime 2018

In this guide, I', gonna tell you How To Install TWRP Recovery On Huawei Y7 Prime 2018 and also root it as well. With TWRP, you can take a full backup of your device data and most importantly flash custom ROMs on the device. Smartphone enthusiasts these days have a lot of affinity for customizing their phones.

## What is TWRP
TWRP is the shortened form for the <b>TeamWin Recovery Project</b>. It is a fully touch-based and open source custom recovery for Android devices. It provides a touchscreen-enabled interface that enables the users to install third-party and customized firmware. Also, users can back up their device, which is not unsupported by stock recovery images.

Now let’s see what are some advantages of installing TWRP on the device. It allows you to install custom ROMs, recoveries, Xposed Modules, and other modifications. This is not all. You can also root our phone, create Nandroid backups, over-clock to improve the efficiency of your device.So, let’s check out how to install TWRP recovery on Huawei Y7 Prime 2018.

## Pre-Requisite: 
 * Get the battery charge your phone to at least 70%
 * Install all of required drivers for your operating system. [Get driver](https://gitlab.com/farhansadik/huawei-y7-prime-root-instruction/-/tree/master/Driver)
 * Unlock Device Bootloader | [unlock method](http://squaredevops.blogspot.com/2017/11/how-to-unlock-bootloader-on-lava-iris.html)
 * TWRP Recovery Image File 
 * SuperUSER Root 

> Follow this guide carefully and install the custom recovery at your own risk. I will not be responsible for bricked devices or other hardware & software related issues arising due to faulty installation.

## TWRP Installation 
Files are developed by chinese developers, so there’s limited support for this device so far. Your warranty will be void after root. Follow at your own risk, i don’t take any responsibility if anything goes wrong.

<b>Supported Devices: </b><br>
tested devices: TRT-AL00, TRT-AL00A, TRT-TL10, TRT-TL10A, TRT-L21A, TRT-LX1, TRT-LX3 <br>
not tested devices: TRT-LX2, TRT-L03, TRT-L53 <br>

> Before you go, please unlock your [bootloader](http://squaredevops.blogspot.com/2017/11/how-to-unlock-bootloader-on-lava-iris.html)

1. Backup all important data<br>
2. Download TWRP [Image Files](https://gitlab.com/farhansadik/huawei-y7-prime-root-instruction/-/blob/master/Huawei%20Y7%20TRT.rar) <br>
3. Extract downloaded file on your computer <br>
4. Power off phone, Hold volume down and insert cable. phone should enter fastboot mode. <br>
5. Open <b>MAF32</b> and type `fastboot flash recovery TRT-TWRP-3.1.1-0818.img` and press enter <br>
6. Once installation completed. Disconnect from pc. and hold volume up and power, keep holding until you see TWRP screen. <br>
7. Recovery will be in <b>chinese</b>, Tap on right side button, and choose 2nd option, recovery will be changed to <b>English. </b><br>

> Note: on some devices touch screen does not work in TWRP mode, you are advised to use mouse with OTG connector to work in TWRP

## Rooting Method 
1. Now swipe to allow modifications. (Advised to make backup in TWRP for safe side) <br>
2. Then tap on advanced. and tap root button. <br>
3. Swipe to install root. <br>
4. Reboot phone to normal mode. <br>
5. After several boots , your phone should be up and running. <br>
6. Now go in `settings > Application manager > super su > permissions` and allow all permissions. <br>
7. Done, now install any root checker to verify root access.  <br>

Sources :
1. XDA Developers - https://forum.xda-developers.com/huawei-y7/how-to/twrp-recovery-root-t3734325
2. Square Dev Ops - https://squaredevops.blogspot.com/2020/04/rooting-method-of-huawei-y7-prime.html

Farhan Sadik<br>
<i>Square Development Group</i>
